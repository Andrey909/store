<?php

namespace App\Http\Controllers;
use App\Page;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function show(Page $page){

        return view('pages.show', compact('page'));
    }


    public function create(){
        return view('pages.create');
    }

    public function store()
    {
        $this->validate(request(),[
            'title' => 'required|min:4|unique:pages,title',
            'alias' => 'required|min:2|max:20|unique:pages,alias',
            'intro' => 'required',
            'content' => 'required'
        ]);
        Page::create(request(['title', 'alias',  'intro', 'content']));

        //redirect to home page

        return redirect('/pages');
    }


    public function edit(Page $page){
        return view('pages.edit', compact('page'));
    }




    public function update(Page $page){

        $this->validate(request(),[
            'title' => 'required|min:4|unique:products,title,',
            'alias' => 'required|min:2|max:20|unique:products,alias,' . $page->id,
            'intro' => 'required',
            'content' => 'required'
        ]);


        $page->update(request(['title', 'alias', 'intro', 'content' ]));

        return redirect('/pages');

    }

    public function delete(Page $page){

        return view('pages.delete', compact('page'));
    }


    public function destroy(Page $page){
        $page->delete();
        return redirect('/pages');
    }
}
