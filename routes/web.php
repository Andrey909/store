<?php


Route::get('/','HomeController@home');



Route::get('/products/{product}/delete' , 'ProductsController@delete');
Route::get('/pages/{page}/delete' , 'PagesController@delete');

Route::resources([
        'pages' => 'PagesController',
        'products' => 'ProductsController'

        ]);

/*Route::get('/products/create', 'ProductsController@create');
 * Route::get('/products/{product}','ProductsController@show');
Route::post('/products', 'ProductsController@store');
Route::delete('/products/{product}', 'ProductsController@destroy');
Route::get('/products/{product}/edit', 'ProductsController@edit');
Route::patch('/products/{product}','ProductsController@update');*/


/*
Route::get('/','HomeController@home');

Route::get('/posts', function(){
    dd('posts');
});

Route::get('/products/create', 'ProductsController@create');
Route::get('/products/{alias}','ProductsController@show');
Route::post('/products', 'ProductsController@store');
Route::get('/products/{alias}/edit', 'ProductsController@edit');
Route::post('/products/{alias}','ProductsController@update');
Route::get('/products/{alias}/delete' , 'ProductsController@delete');
Route::post('/products/{alias}/destroy', 'ProductsController@destroy');*/