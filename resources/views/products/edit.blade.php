@extends('template')

@section('content')

    <div class="col-md-12">

        <form action="/products/{{$product->alias}}" method="post" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}

            <div class="form-group">

                <label for="title">Title:</label>
                <input type="text" value="{{$product['title']}}" name="title" id="title" class="form-control">

            </div>

            <div class="form-group">

                <label for="alias">Alias:</label>
                <input type="text" value="{{$product['alias']}}" name="alias" id="alias" class="form-control">

            </div>

            <div class="form-group">

                <label for="price">Price:</label>
                <input type="text" name="price" value="{{$product['price']}}" id="price" class="form-control">

            </div>

            <div class="form-group">

                <label for="description">Description</label>
                <textarea name="description" id="description" class="form-control">{{$product['description']}}</textarea>

            </div>

            <div class="form-group">
                <button class="btn btn-default">Update</button>
            </div>

        </form>

    </div>

@endsection

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Update product:</h1>
        </div>
    </div>
@endsection