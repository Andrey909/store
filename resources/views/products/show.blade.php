@extends('template')

@section('content')
    <div class="col-md-12">
        <h2>{{ $product['title'] }}</h2>
        <p>Цена: {{ $product['price'] }} грн.</p>
        <p> {{ $product['description'] }} </p>
    </div>


@endsection

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-5">{{ $product['title'] }}</h1>
        </div>
    </div>
@endsection