@extends('template')

@section('content')

    <div class="col-md-12">

        <form action="/pages/{{$page->alias}}" method="post" class="form-horizontal">

            @include('embed.errors')

            {{csrf_field()}}

            <div class="form-group">

                <label for="title">Title:</label>
                <input type="text" value="{{$page['title']}}" name="title" id="title" class="form-control">

            </div>

            <div class="form-group">

                <label for="alias">Alias:</label>
                <input type="text" value="{{$page['alias']}}" name="alias" id="alias" class="form-control">

            </div>

            <div class="form-group">

                <label for="intro">Intro:</label>
                <input type="text" name="price" value="{{$page['intro']}}" id="price" class="form-control">

            </div>

            <div class="form-group">

                <label for="content">content</label>
                <textarea name="content" id="content" class="form-control">{{$page['content']}}</textarea>

            </div>

            <div class="form-group">
                <button class="btn btn-default">Update</button>
            </div>

        </form>

    </div>

@endsection

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-4">Update page:</h1>
        </div>
    </div>
@endsection