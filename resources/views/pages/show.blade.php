@extends('template')

@section('content')
    <div class="col-md-12">
        <h2>{{ $page['title'] }}</h2>
        <p>Цена: {{ $page['intro'] }} грн.</p>
        <p> {{ $page['content'] }} </p>
    </div>


@endsection

@section('jumbotron')
    <div class="jumbotron">
        <div class="container">
            <h1 class="display-5">{{ $page['title'] }}</h1>
        </div>
    </div>
@endsection