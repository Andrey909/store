<nav class="nav d-flex justify-content-between">

    <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>


    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</a>
        <div class="dropdown-menu" aria-labelledby="dropdown01">
            <a class="dropdown-item" href="/pages/create">Create</a>
            <a class="dropdown-item" href="/pages">Show</a>
        </div>
    </li>

    <a class="nav-link" href="/products/create">Add new Product <span class="sr-only">(current)</span></a>
</nav>

